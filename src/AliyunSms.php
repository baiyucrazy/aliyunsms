<?php

namespace Aliyun\LaravelAliyunSms;


class AliyunSms
{
    //阿里云短信的appkey
    protected $appkey;

    //阿里云短信的秘钥
    protected $secretkey;

    //阿里云短信签名
    protected $sms_free_sign_name;

    //短信类型
    protected $sms_type;

    //短信模板id的数组
    protected $sms_template_code_list;

    //阿里云对象
    protected $client;

    /**
     * AliyunSms constructor.
     * @param int $gatewayType   选择用哪个环境，默认用正式环境
     */
    public function __construct($gatewayType = 1)
    {
        require_once __DIR__ . '/TopSdk.php';
        $this->appkey = config('aliyunSms.appkey');
        $this->secretkey = config('aliyunSms.secretkey');
        $this->client =  new \TopClient($this->appkey,$this->secretkey);
        $this->client->format = 'json';         //返回结果的格式

        if ($gatewayType == 1){
            //正式环境
            $this->client->gatewayUrl = 'http://gw.api.taobao.com/router/rest';
        }else{
            //沙箱环境
            $this->client->gatewayUrl = 'http://gw.api.tbsandbox.com/router/rest';
        }

        $this->sms_free_sign_name = config('aliyunSms.sms_free_sign_name');
        $this->sms_type = config('aliyunSms.sms_type', 'normal');
        $this->sms_template_code_list = config('aliyunSms.sms_template_code_list');
    }

    /**
     * 发送信息的方法
     * @param array $rec_num 接受信息的电话号码数组
     * @param array $sms_param 根据信息的模板生成的信息内容数组
     * @param $sms_template_code   信息模板的
     * @return bool
     */
    public function sendMsg(array $rec_num, array $sms_param, $sms_template_code)
    {
        $req = new \AlibabaAliqinFcSmsNumSendRequest();

        if (empty($rec_num)){
            return false;
        }else{
            $req->setRecNum(implode(',', $rec_num));
        }

        if (empty($sms_param)){
            return false;
        }else{
            $req->setSmsParam(json_encode($sms_param));
        }

        if (empty($sms_template_code || !in_array($sms_template_code, $this->sms_template_code_list))){
            return false;
        }else{
            $req->setSmsTemplateCode($sms_template_code);
        }

        $req->setSmsType($this->sms_type);
        $req->setSmsFreeSignName($this->sms_free_sign_name);

        $result = $this->client->execute($req);

        if (!empty($result->result) && $result->result->success){
            return true;
        }

        \Log::info('短息错误log', $this->object_to_array($result));

        return false;
    }

    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    protected function object_to_array($obj) {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }

        return $obj;
    }

}

