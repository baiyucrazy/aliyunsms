<?php

namespace Aliyun\LaravelAliyunSms;

use Illuminate\Support\ServiceProvider;

class AliyunSmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/../config/aliyunSms.php';
        if (function_exists('config_path')){
            $publishPath = config_path('aliyunSms.php');
        }else{
            $publishPath = base_path('config/aliyunSms.php');
        }

        $this->publishes([$configPath => $publishPath], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/aliyunSms.php';
        $this->mergeConfigFrom($configPath, 'aliyunSms');
    }
}
