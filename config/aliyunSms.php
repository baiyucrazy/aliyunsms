<?php

return [

    /**
     * 阿里云短信的appkey
     */
    'appkey' => null,

    /**
     * 阿里云短信的秘钥
     */
    'secretkey' => null,

    /**
     * 阿里云短信签名，传入的短信签名必须是在阿里大于“管理中心-短信签名管理”中的可用签名
     */
    'sms_free_sign_name' => null,

    /**
     * //短信类型，传入值请填写normal
     */
    'sms_type' => 'normal',

    /**
     * 短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板。
     */
    'sms_template_code_list' => [],

];
